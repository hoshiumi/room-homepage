const homeBg = document.querySelector('.home-bg')
const movers = document.querySelectorAll('.mover')
const images = document.querySelectorAll('.home-bg > *')
const menuBtns = document.querySelectorAll('.menu')
const navbar = document.querySelector('.navbar')
const breakpoint = 1024

let i = 0

// window.addEventListener('DOMContentLoaded', () => {
//     if (window.innerWidth > breakpoint) {
//         homeBg.style.backgroundImage = `url(${root + desktopImages[i]})`
//     } else {
//         homeBg.style.backgroundImage = `url(${root + mobileImages[i]})`
//     }
// })
// window.addEventListener('resize', () => {
//     if (window.innerWidth > breakpoint) {
//         homeBg.style.backgroundImage = `url(${root + desktopImages[i]})`
//     } else {
//         homeBg.style.backgroundImage = `url(${root + mobileImages[i]})`
//     }
// })

movers.forEach((mover) => {
    mover.addEventListener('click', (e) => {
        console.log(e)
        if (e.target.getAttribute('data-move') == 'left') {
            if (i <= 0) {
                i = 3
            }
            i--
        }

        if (e.target.getAttribute('data-move') == 'right') {
            if (i == images.length - 1) {
                i = -1
            }
            i++
        }
        animate()

        console.log(i)
    })
})

setInterval(() => {
    if (i == images.length - 1) {
        i = -1
    }
    i++
    animate()
}, 3500)

menuBtns.forEach((menuBtn) => {
    menuBtn.addEventListener('click', (e) => {
        if (menuBtn.classList.contains('menu-open')) {
            navbar.classList.add('active')
        }
        if (menuBtn.classList.contains('menu-close')) {
            navbar.classList.remove('active')
        }

        menuBtns.forEach((menu) => menu.classList.remove('inactive'))
        menuBtn.classList.add('inactive')
    })
})
function animate() {
    images.forEach((image) => {
        image.classList.add('none')
    })

    setTimeout(() => {
        images[i].classList.remove('none')
    }, 400)
}
